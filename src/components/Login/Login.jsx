import { Link, useNavigate } from 'react-router-dom';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

import '../../common/styles/AuthForm.css';
import ErrorList from '../../common/ErrorList/ErrorList';
import { useState } from 'react';
import coursesAppBackend from '../../helpers/coursesAppBackend';

const Login = (props) => {
	const navigate = useNavigate();
	const [errorMessages, setErrorMessages] = useState([]);

	const handleLoginSubmit = (event) => {
		event.preventDefault();

		const requestBody = {
			email: event.target.elements.email.value,
			password: event.target.elements.password.value,
		};

		coursesAppBackend
			.post('login', requestBody)
			.then((res) => {
				if (res.data.successful) {
					props.loginHandler(res.data.result, res.data.user);
					navigate('/courses');
				}
			})
			.catch((err) => {
				setErrorMessages(
					err.response.data.errors || [err.response.data.result] || []
				);
			});
	};

	return (
		<div className='AuthForm_wrapper'>
			<form className='AuthForm' onSubmit={handleLoginSubmit}>
				<h2>Login</h2>
				<Input
					id='email'
					name='email'
					labelText='Email'
					placeholderText='Enter email'
				/>
				<Input
					id='password'
					name='password'
					type='password'
					labelText='Password'
					placeholderText='Enter password'
				/>
				<Button buttonText='Login' type='submit' />
				<ErrorList errors={errorMessages} />
				<p>
					If you don't have an account you can{' '}
					<Link to='/registration'>Sign up</Link>.
				</p>
			</form>
		</div>
	);
};

export default Login;
