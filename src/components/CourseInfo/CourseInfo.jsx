import { Link, useParams } from 'react-router-dom';

import pipeDuration from '../../helpers/pipeDuration';
import extractAuthorName from '../../helpers/extractAuthorName';
import { mockedCoursesList } from '../../constants';

import './CourseInfo.css';

const CourseInfo = (props) => {
	const { courseId } = useParams();
	const course = mockedCoursesList.find((course) => course.id === courseId);
	return (
		<div className='CourseInfo'>
			<Link to='/courses' className='CourseInfo_link_back'>
				&lt; Back to courses
			</Link>
			<h2 className='text-center'>{course.title}</h2>
			<div className='CourseInfo_main_grid'>
				<div className='CourseInfo_block'>
					<p>{course.description}</p>
				</div>
				<div className='CourseInfo_block'>
					<ul className='CourseInfo_details_list'>
						<li>
							<b>ID</b>: {courseId}
						</li>
						<li>
							<b>Duration</b>: {pipeDuration(course.duration)}
						</li>
						<li>
							<b>Created</b>: {course.creationDate}
						</li>
						<li>
							<ul className='CourseInfo_details_list CourseInfo_authors'>
								<li>
									<b>Authors</b>:
								</li>
								{course.authors.map((author) => (
									<li key={author}>{extractAuthorName(author)}</li>
								))}
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
