import { Link, useNavigate } from 'react-router-dom';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';

import '../../common/styles/AuthForm.css';
import coursesAppBackend from '../../helpers/coursesAppBackend';
import { useState } from 'react';
import ErrorList from '../../common/ErrorList/ErrorList';

const Registration = (props) => {
	const navigate = useNavigate();
	const [errorMessages, setErrorMessages] = useState([]);

	const handleRegistrationSubmit = (event) => {
		event.preventDefault();

		const newUser = {
			name: event.target.elements.name.value,
			email: event.target.elements.email.value,
			password: event.target.elements.password.value,
		};

		coursesAppBackend
			.post('register', newUser)
			.then((res) => {
				if (res.data.successful) {
					navigate('/login');
				}
			})
			.catch((err) => {
				setErrorMessages(
					err.response.data.errors || [err.response.data.result] || []
				);
			});
	};

	return (
		<div className='AuthForm_wrapper'>
			<form className='AuthForm' onSubmit={handleRegistrationSubmit}>
				<h2>Registration</h2>
				<Input
					id='name'
					name='name'
					labelText='Name'
					placeholderText='Enter name'
				/>
				<Input
					id='email'
					name='email'
					type='email'
					labelText='Email'
					placeholderText='Enter email'
				/>
				<Input
					id='password'
					name='password'
					type='password'
					labelText='Password'
					placeholderText='Enter password'
				/>
				<Button buttonText='Registration' type='submit' />
				<ErrorList errors={errorMessages} />
				<p>
					If you have an account you can <Link to='/login'>Login</Link>.
				</p>
			</form>
		</div>
	);
};

export default Registration;
