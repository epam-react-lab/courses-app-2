import { useNavigate } from 'react-router-dom';

import Button from '../../../../common/Button/Button';
import extractAuthorName from '../../../../helpers/extractAuthorName';
import pipeDuration from '../../../../helpers/pipeDuration';

import './courseCard.css';

const CourseCard = (props) => {
	const navigate = useNavigate();

	const showCourse = () => {
		navigate(`/courses/${props.course.id}`);
	};

	return (
		<div className='CourseCard'>
			<div className='CourseCard_column'>
				<h3>{props.course.title}</h3>
				<p>{props.course.description}</p>
			</div>
			<div className='CourseCard_column'>
				<p className='CourseCard_authors'>
					<b>Authors</b>:
					{props.course.authors
						.map((authorId) => extractAuthorName(authorId))
						.join(', ')}
				</p>
				<p>
					<b>Duration</b>:{pipeDuration(props.course.duration)}
				</p>
				<p>
					<b>Created</b>:{props.course.creationDate}
				</p>
				<div className='CourseCard_showBtnContainer'>
					<Button buttonText='Show course' onClick={showCourse} />
				</div>
			</div>
		</div>
	);
};

export default CourseCard;
