import { useState } from 'react';

import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';
import Button from '../../common/Button/Button';
import { mockedCoursesList } from '../../constants';

import './courses.css';
import { useNavigate } from 'react-router-dom';

const Courses = (props) => {
	let searchString = '';
	const search = (value) => {
		searchString = value;

		return mockedCoursesList.filter((course) => {
			return (
				course.id === searchString ||
				course.title.toLowerCase().indexOf(searchString.toLowerCase()) !== -1
			);
		});
	};

	const [courses, setCourses] = useState(search(searchString));
	const navigate = useNavigate();

	const createCourse = () => {
		navigate('/courses/add');
	};

	return (
		<div className='Courses'>
			<div className='Courses_header'>
				<SearchBar onSearch={(value) => setCourses(search(value))} />
				<Button buttonText='Add new course' onClick={createCourse} />
			</div>
			<div className='Courses_container'>
				{courses.map((course) => (
					<CourseCard key={course.id} course={course} />
				))}
			</div>
		</div>
	);
};

export default Courses;
