import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';

import './Header.css';

const Header = (props) => {
	return (
		<div className='Header'>
			<span>
				<Logo height='30px' />
			</span>
			{props.authMode && (
				<span className='NavElements'>
					<span>{props.username}</span>
					<Button buttonText='Logout' onClick={props.logoutHandler} />
				</span>
			)}
		</div>
	);
};

export default Header;
