import axios from 'axios';

import { API_URL } from '../constants';

const retrieveAuthToken = () => {
	return localStorage.getItem('Authorization') || '';
};

const coursesAppBackend = axios.create({
	baseURL: API_URL,
	timeout: 1000,
	headers: { Authorization: retrieveAuthToken() },
});

export default coursesAppBackend;
