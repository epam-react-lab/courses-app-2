import './errorList.css';

const ErrorList = (props) => {
	return (
		props.errors.length !== 0 && (
			<ul className='ErrorList'>
				{props.errors.map((error, i) => (
					<li key={i} className='ErrorList_item'>
						{error}
					</li>
				))}
			</ul>
		)
	);
};

export default ErrorList;
