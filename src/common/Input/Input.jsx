import './input.css';

const Input = (props) => {
	return (
		<span>
			<label className='Input_label' htmlFor={props.id}>
				{props.labelText}
			</label>
			<input
				className='Input bs-bb w-100'
				id={props.id}
				name={props.name}
				type={props.type || 'text'}
				placeholder={props.placeholderText}
				onChange={props.onChange ? (e) => props.onChange(e) : null}
			/>
		</span>
	);
};

export default Input;
