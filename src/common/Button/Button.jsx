import './Button.css';

const Button = (props) => {
	return (
		<button
			className='Button'
			type={props.type || 'button'}
			onClick={props.onClick}
		>
			{props.buttonText}
		</button>
	);
};

export default Button;
