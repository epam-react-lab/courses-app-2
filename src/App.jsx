import { Routes, Route, Navigate, useNavigate } from 'react-router-dom';

import Header from './components/Header/Header';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import CourseInfo from './components/CourseInfo/CourseInfo';
import PageNotFound from './components/PageNotFound/PageNotFound';
import { useState } from 'react';
import coursesAppBackend from './helpers/coursesAppBackend';

const App = () => {
	const [userData, setUserData] = useState({});
	const navigate = useNavigate();

	const fetchUserData = () => {
		coursesAppBackend
			.get('users/me')
			.then((res) => {
				setUserData({
					name: res.data.result.name,
					email: res.data.result.email,
				});
			})
			.catch((err) => {
				localStorage.setItem('Authorization', '');
			});
	};

	const authToken = () => {
		const token = localStorage.getItem('Authorization') || '';
		if (token !== '' && userData.email === undefined) {
			fetchUserData();
		}
		return token;
	};

	const login = (newToken, user) => {
		localStorage.setItem('Authorization', newToken);
		setUserData({
			name: user.name,
			email: user.email,
		});
	};

	const logout = () => {
		localStorage.removeItem('Authorization');
		setUserData({});
		navigate('/login');
	};

	const isLoggedIn = () => {
		if (authToken() === '') return false;
		if (userData.email === undefined) {
			fetchUserData();
		}
		return true;
	};
	return (
		<>
			<Header
				authMode={isLoggedIn()}
				username={userData.name}
				logoutHandler={logout}
			/>
			<Routes>
				<Route
					path='/registration'
					element={isLoggedIn() ? <Navigate to='/courses' /> : <Registration />}
				/>
				<Route
					path='/login'
					element={
						isLoggedIn() ? (
							<Navigate to='/courses' />
						) : (
							<Login loginHandler={login} />
						)
					}
				/>
				<Route path='/courses'>
					<Route
						index
						element={isLoggedIn() ? <Courses /> : <Navigate to='/login' />}
					/>
					<Route
						path='add'
						element={isLoggedIn() ? <CreateCourse /> : <Navigate to='/login' />}
					/>
					<Route
						path=':courseId'
						element={isLoggedIn() ? <CourseInfo /> : <Navigate to='/login' />}
					/>
				</Route>
				<Route path='/' element={<Navigate to='/courses' />} />
				<Route path='*' element={<PageNotFound />} />
			</Routes>
		</>
	);
};

export default App;
